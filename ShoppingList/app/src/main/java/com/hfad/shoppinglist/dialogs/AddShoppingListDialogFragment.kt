package com.hfad.shoppinglist.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import com.hfad.shoppinglist.R
import com.hfad.shoppinglist.database.ShoppingList


class AddShoppingListDialogFragment(var addDialogListener: AddDialogListener): DialogFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.dialog_add_shopping_list, container, false)

        val etName: EditText = root.findViewById(R.id.etName)
        val tvAdd: TextView = root.findViewById(R.id.tvAdd)
        val tvCancel: TextView = root.findViewById(R.id.tvCancel)

        tvAdd.setOnClickListener {
            val name = etName.text.toString()
            if (name.isNullOrEmpty()) {
                Toast.makeText(context, "Please enter a name", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            val currentDateTime = System.currentTimeMillis()

            val list = ShoppingList(shoppingListTitle = name, dateOfList = currentDateTime, isArchived = false)
            addDialogListener.onAddButtonClicked(list)
            dismiss()
        }

        tvCancel.setOnClickListener {
            dismiss()
        }

        return root
    }

}