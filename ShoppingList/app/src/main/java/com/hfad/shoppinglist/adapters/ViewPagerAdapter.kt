package com.hfad.shoppinglist.adapters

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.hfad.shoppinglist.R
import com.hfad.shoppinglist.ui.main.ArchivedListsFragment
import com.hfad.shoppinglist.ui.main.ListsFragment

private val TAB_TITLES = arrayOf(
        R.string.tab_title_1,
        R.string.tab_title_2
)

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
class SectionsPagerAdapter(private val fragmentManager: FragmentManager, lifecycle: Lifecycle)
    : FragmentStateAdapter(fragmentManager, lifecycle) {

    override fun getItemCount(): Int {
        return 2
    }

    override fun createFragment(position: Int): Fragment {
        return when (position){
            0 -> ListsFragment()
            1 -> ArchivedListsFragment()
            else -> ListsFragment()
        }
    }
}