package com.hfad.shoppinglist.database

import androidx.room.*

@Entity
data class ShoppingList (

    @PrimaryKey(autoGenerate = true)
    var listId: Long = 0L,

    val shoppingListTitle: String,

    val dateOfList: Long,

    val isArchived: Boolean
)

