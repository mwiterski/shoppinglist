package com.hfad.shoppinglist

import java.text.SimpleDateFormat
import java.util.*

fun convertLongToStringTime(time: Long): String {
    val date = Date(time)
    val format = SimpleDateFormat("yyyy.MM.dd")
    return format.format(date)
}