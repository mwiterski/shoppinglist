package com.hfad.shoppinglist.dialogs

import com.hfad.shoppinglist.database.ShoppingItem

interface AddDialogListenerItem {
    fun onAddButtonClicked(item: ShoppingItem)
}