package com.hfad.shoppinglist.dialogs

import com.hfad.shoppinglist.database.ShoppingList

interface AddDialogListener {
    fun onAddButtonClicked(item: ShoppingList)
}