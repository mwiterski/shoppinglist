package com.hfad.shoppinglist.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.hfad.shoppinglist.R
import com.hfad.shoppinglist.ui.main.ShoppingListDetailsActivity
import com.hfad.shoppinglist.convertLongToStringTime
import com.hfad.shoppinglist.database.ShoppingList


class ShoppingListAdapter: RecyclerView.Adapter<ShoppingListAdapter.ViewHolder>() {

    var data = listOf<ShoppingList>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data[position]
        holder.bind(item)

        /*this is the easiest way to handle item clicks
        however it's bad for performance
        TODO: place setOnClickListener outside of onBindViewHolder, using additional interface*/
        holder.itemView.setOnClickListener(View.OnClickListener {

            val intent= Intent(it.context, ShoppingListDetailsActivity::class.java)
            intent.putExtra("listId", item.listId)
            it.context.startActivity(intent)

        })

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }
    class ViewHolder private constructor(itemView: View) : RecyclerView.ViewHolder(itemView){
        val listTitle: TextView = itemView.findViewById(R.id.shopping_list_title)
        val date: TextView = itemView.findViewById(R.id.shopping_date)

        fun bind(item: ShoppingList) {
            val res = itemView.context.resources
            listTitle.text = item.shoppingListTitle
            date.text = convertLongToStringTime(item.dateOfList)
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val view = layoutInflater
                    .inflate(R.layout.list_item_shopping_list, parent, false)

                return ViewHolder(view)
            }
        }
    }
}