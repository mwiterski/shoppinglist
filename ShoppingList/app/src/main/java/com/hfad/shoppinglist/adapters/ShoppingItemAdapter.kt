package com.hfad.shoppinglist.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.hfad.shoppinglist.R
import com.hfad.shoppinglist.database.ShoppingItem


class ShoppingItemAdapter: RecyclerView.Adapter<ShoppingItemAdapter.ViewHolder>() {

    var data = listOf<ShoppingItem>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data[position]
        holder.bind(item)

        /*this is the easiest way to handle item clicks
        however it's bad for performance
        TODO: place setOnClickListener outside of onBindViewHolder, using additional interface*/
        holder.itemView.setOnClickListener(View.OnClickListener {

        })
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }
    class ViewHolder private constructor(itemView: View) : RecyclerView.ViewHolder(itemView){
        val itemName: TextView = itemView.findViewById(R.id.shopping_item_name)
        val amount: TextView = itemView.findViewById(R.id.amount)

        fun bind(item: ShoppingItem) {
            val res = itemView.context.resources
            itemName.text = item.name
            amount.text = item.amount.toString()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val view = layoutInflater
                    .inflate(R.layout.list_item_shopping_item, parent, false)

                return ViewHolder(view)
            }
        }
    }
}