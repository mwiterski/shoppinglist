package com.hfad.shoppinglist.database

import androidx.room.Embedded
import androidx.room.Relation

data class ShoppingListWithItem(
        @Embedded val shoppingList: ShoppingList,
        @Relation(
                parentColumn = "listId",
                entityColumn = "shoppingListId"
        )
        var shoppingItems: List<ShoppingItem>
)