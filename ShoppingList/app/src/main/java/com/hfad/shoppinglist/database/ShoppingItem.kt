package com.hfad.shoppinglist.database
import androidx.room.*

@Entity(tableName = "shopping_item_table")
data class ShoppingItem (
        @PrimaryKey(autoGenerate = true)
        val itemId: Long = 0L,

        var shoppingListId: Long,

        var name: String,

        var amount: String,

        var isBought: Boolean
)