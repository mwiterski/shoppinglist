package com.hfad.shoppinglist.viemodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.hfad.shoppinglist.database.ShoppingListDatabaseDao

class ShoppingListViewModelFactory(
        private val dataSourceList: ShoppingListDatabaseDao,
//        private val dataSourceItem: ShoppingItemDatabaseDao,
        private val application: Application
) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ShoppingListViewModel::class.java)) {
            return ShoppingListViewModel(dataSourceList, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}