package com.hfad.shoppinglist.database

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface ShoppingListDatabaseDao {

    @Insert
    suspend fun insert(shoppingList: ShoppingList)

    @Update
    suspend fun update(shoppingList: ShoppingList)

    @Query("DELETE FROM shoppingList")
    suspend fun clear()

    @Query("SELECT * from shoppingList ORDER BY listId DESC")
    fun getAllShoppingLists(): LiveData<List<ShoppingList>>

    @Query("SELECT * from shoppingList WHERE isArchived = 0 ORDER BY listId DESC")
    fun getAllActiveShoppingLists(): LiveData<List<ShoppingList>>

    @Query("SELECT * from shoppingList WHERE isArchived = 1 ORDER BY listId DESC")
    fun getAllArchivedShoppingLists(): LiveData<List<ShoppingList>>

    @Transaction
    @Query("SELECT * FROM shoppingList WHERE listId = :shoppingListId")
    fun getShoppingListWithItems(shoppingListId: Long):
        LiveData<List<ShoppingListWithItem>>

    @Insert
    suspend fun insertItem(shoppingItem: ShoppingItem, listId: Long) {
        shoppingItem.shoppingListId = listId
    }

}