package com.hfad.shoppinglist.viemodel

import android.app.Application
import androidx.lifecycle.*
import com.hfad.shoppinglist.database.ShoppingItem
import com.hfad.shoppinglist.database.ShoppingList
import com.hfad.shoppinglist.database.ShoppingListDatabaseDao
import com.hfad.shoppinglist.database.ShoppingListWithItem
import kotlinx.coroutines.launch

class ShoppingListViewModel(
    dataSourceList: ShoppingListDatabaseDao,
    application: Application) : ViewModel() {

    val databaseList = dataSourceList

    val allShoppingLists = databaseList.getAllShoppingLists()
    val allActiveShoppingList = databaseList.getAllActiveShoppingLists()
    val allArchivedShoppingList = databaseList.getAllArchivedShoppingLists()

    private val _detailShoppingListId = MutableLiveData<Long>()
    val detailShoppingListId: LiveData<Long>
        get() = _detailShoppingListId

    val allShoppingListWithItems =
        _detailShoppingListId.value?.let { databaseList.getShoppingListWithItems(it) }

    private val _allItemsOnDetailList = allShoppingListWithItems?.let { getItemsForListId(it) }
    val allItemsOnDetailList: LiveData<List<ShoppingItem>>?
        get() = _allItemsOnDetailList


    private suspend fun insert(list: ShoppingList) {
        databaseList.insert(list)
    }

    fun addShoppingList(list: ShoppingList) {
        viewModelScope.launch {
            insert(list)
        }
    }

    fun getDetailListId(listId: Long) {
        _detailShoppingListId.value = listId
    }

    fun getItemsForListId(listsWithItems: LiveData<List<ShoppingListWithItem>>):
            LiveData<List<ShoppingItem>> {

        var items = MutableLiveData<List<ShoppingItem>>()
        items.value = listsWithItems.value?.get(0)?.shoppingItems

        return items
    }






}