package com.hfad.shoppinglist.ui.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.hfad.shoppinglist.R
import com.hfad.shoppinglist.adapters.ShoppingListAdapter
import com.hfad.shoppinglist.database.ShoppingList
import com.hfad.shoppinglist.database.ShoppingListDatabase
import com.hfad.shoppinglist.databinding.FragmentShoppingListBinding
import com.hfad.shoppinglist.dialogs.AddDialogListener
import com.hfad.shoppinglist.dialogs.AddShoppingListDialogFragment
import com.hfad.shoppinglist.viemodel.ShoppingListViewModel
import com.hfad.shoppinglist.viemodel.ShoppingListViewModelFactory


class ListsFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Get a reference to the binding object and inflate the fragment views.
        val binding: FragmentShoppingListBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_shopping_list, container, false)

        val application = requireNotNull(this.activity).application
        val dataSource = ShoppingListDatabase.getInstance(application).shoppingListDatabaseDao
        val viewModelFactory = ShoppingListViewModelFactory(dataSource, application)

        val shoppingListViewModel =
            ViewModelProvider(
                this, viewModelFactory).get(ShoppingListViewModel::class.java)

        binding.lifecycleOwner = this
        binding.shoppingListViewModel = shoppingListViewModel
        binding.fab.setOnClickListener {
            AddShoppingListDialogFragment(
                object : AddDialogListener {
                    override fun onAddButtonClicked(list: ShoppingList) {
                        shoppingListViewModel.addShoppingList(list)
                    }
                }).show(childFragmentManager, "AddShoppingListTag")
        }

        val adapter = ShoppingListAdapter()

        binding.shoppingList.adapter = adapter

        shoppingListViewModel.allActiveShoppingList.observe(viewLifecycleOwner, Observer {
            it?.let {
                adapter.data = it
            }
        })

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

}